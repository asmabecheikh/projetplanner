// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyBmWHt4iJXXPxfRYkoHpNMljyOgmNyDC_c",
    authDomain: "projectplanner-b3f49.firebaseapp.com",
    databaseURL: "https://projectplanner-b3f49.firebaseio.com",
    projectId: "projectplanner-b3f49",
    storageBucket: "projectplanner-b3f49.appspot.com",
    messagingSenderId: "623222037182",
    appId: "1:623222037182:web:9f6b83ac0e202a4b6c9cef",
    measurementId: "G-6CF8ZHWHM5"
  
  } 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

