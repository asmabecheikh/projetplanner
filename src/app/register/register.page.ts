import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Location } from '@angular/common';

import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { ProfileTypes } from '../enumerations/profile-types';
import { PROFILE_TYPES } from '../enumerations/profile-types.constant';
import { User } from '../models/user.model';
import { Storage } from '@ionic/storage';
import { CURRENT_USER_KEY } from '../Actor/storage.constant';

/* enum ProfileTypes {
  CLIENT = 'CLIENT',
  CHEF_PROJET = 'CHEF_PROJET',
  DEVELOPPEUR = 'DEVELOPPEUR'
}

const PROFILE_TYPES: ProfileTypes[] = [
  ProfileTypes.CLIENT,
  ProfileTypes.CHEF_PROJET,
  ProfileTypes.DEVELOPPEUR
]; */

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  username: string = "";
  password: string = "";
  cpassword: string = "";

  profileType: ProfileTypes = null;
  profileTypes: ProfileTypes[] = PROFILE_TYPES.slice(0);
  currentUser: User = new User();

  constructor(
    public afAuth: AngularFireAuth,
    public alertController: AlertController,
    public router: Router,
    public afstore: AngularFirestore,
    public user: UserService,
    private location: Location,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  async presentAlert(title: string, content: string) {
    const alert = await this.alertController.create({
      header: title,
      message: content,
      buttons: ['OK']
    })

    await alert.present()
  }

  async signUp() {
    const { username, password, cpassword } = this
        if (password !== cpassword) {
          this.presentAlert('Alerte', 'Les mots de passe ne correspondent pas!')
          return console.error("Passwords don't match")
        } 

    try {

      const res = await this.afAuth.auth.createUserWithEmailAndPassword(username, password).then(
        (result: firebase.auth.UserCredential) => {
          const USER = new User();
          USER.id = result.user.uid;
          USER.username = this.username
          USER.profileType = this.profileType;
          console.log("user:", USER)
          this.user.addUser(USER).then(
            () => {
              this.storage.set(CURRENT_USER_KEY, USER);
              this.presentAlert('Succès', 'Vous êtes inscrit!')
              this.navigateToCurrentUserPage()
            }
          )
        })

      //this.router.navigate(['/tabs'])

    } catch (error) {
      console.dir(error)
      if (error.code === "auth/email-already-in-use") {
        this.presentAlert('Alerte', 'L\'adresse e-mail est déjà utilisée par un autre compte')
        console.log("User not found");
      }
    }
  }

  goBack() {
    this.location.back();
  }

  onProfileTypeSelected(profileType: ProfileTypes) {

    this.profileType = profileType;
    console.log("profile type", profileType);
  }

  navigateToCurrentUserPage() {

    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.currentUser = currentUser;
        this.router.navigate(['/' + this.currentUser.profileType.toLowerCase().replace('_', '-')+"/tabs/tab1"]);
      })

  }


}
