import { Component, OnInit, Input } from '@angular/core';
import { NavParams } from "@ionic/angular";
import { ModalController } from "@ionic/angular";

@Component({
  selector: 'app-detailmodal',
  templateUrl: './detailmodal.page.html',
  styleUrls: ['./detailmodal.page.scss'],
})
export class DetailmodalPage implements OnInit {
  @Input() item;
  constructor(private modalController: ModalController) { }

  ngOnInit() {
    console.log(this.item);
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
