import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalTachePage } from './modalTache.page';

const routes: Routes = [
  {
    path: '',
    component: ModalTachePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalPageRoutingModule {}
