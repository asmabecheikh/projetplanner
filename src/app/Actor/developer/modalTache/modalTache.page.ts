import { Component, OnInit } from "@angular/core";
import { Service } from "../../../service/service";
import { ModalController } from "@ionic/angular";
import { AngularFirestore } from "@angular/fire/firestore";
import { UserService } from "../../../service/user.service";
import { firestore } from "firebase/app";
import { User } from "../../../models/user.model";
import { CURRENT_USER_KEY } from "../../storage.constant";
import { Storage } from "@ionic/storage";
import { CrudprojectServiceService } from 'src/app/service/crudproject-service.service';

@Component({
  selector: "app-modalTache",
  templateUrl: "./modalTache.page.html",
  styleUrls: ["./modalTache.page.scss"]
})
export class ModalTachePage implements OnInit {
  tasks: any;
  nomtache: string = "";
  description: string = "";
  projet: string = "";
  deadline: string = "";
  createur: string = "";
  devemail: string = "";
  etat: string = "";
  projects: any[];
  showme: boolean = false;

  constructor(
    private serviceproject: CrudprojectServiceService,
    private servicetache: Service,
    private modalController: ModalController,
    public afstore: AngularFirestore,
    public user: UserService,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.serviceproject.read_Projets().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nomProjet: e.payload.doc.data()["nomProjet"],
          deadline: e.payload.doc.data()["deadline"],
          description: e.payload.doc.data()["description"],
          budget: e.payload.doc.data()["budget"],
          etat: e.payload.doc.data()["etat"]
        };
      });
    });
  }

  onChange(event) {
    // console.log(event.detail.value.id);
    this.projet = event.detail.value.id;
  }

  crazyEvent(event) {
    this.showme = true;
  }

  async Creertache() {
    const nomtache = this.nomtache;
    const description = this.description;
    const projet = this.projet;
    const deadline = this.deadline;
    const etat = this.etat;

    let tache = {};

    this.storage.get(CURRENT_USER_KEY).then((currentUser: User) => {
      console.log("créateur:" + currentUser.username);
      this.createur = currentUser.username
      this.devemail = currentUser.username
    });
    console.log(this.createur, " + ", this.devemail)
    tache["créateur"] = this.createur;
    tache["Dev"] = this.devemail;
    tache["Nom"] = this.nomtache;
    tache["projet"] = this.projet;
    tache["Deadline"] = this.deadline;
    tache["etat"] = "ToDo";
    tache["description"] = this.description;
    tache["affectaion"] = "affecté";

    this.servicetache.create_Newtask(tache);

    
        this.storage.get(CURRENT_USER_KEY).then((currentUser: User) => {
          console.log("current user id:" + currentUser.id);
    
          this.afstore.doc(`users/${currentUser.id}`).update({
            taches: firestore.FieldValue.arrayUnion({
              nomtache,
              description,
              projet,
              deadline,
              etat,
              //createur,
              //devemail
            })
          });
        }); 

    
    this.modalController.dismiss();
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
