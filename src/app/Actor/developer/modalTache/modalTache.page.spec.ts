import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalTachePage } from './modalTache.page';

describe('ModalTachePage', () => {
  let component: ModalTachePage;
  let fixture: ComponentFixture<ModalTachePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTachePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalTachePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
