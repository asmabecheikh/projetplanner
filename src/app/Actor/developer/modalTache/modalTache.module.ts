import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ModalPageRoutingModule } from "./modalTache-routing.module";

import { ModalTachePage } from "./modalTache.page";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ModalPageRoutingModule],
  declarations: [ModalTachePage]
})
export class ModalPageModule {}
