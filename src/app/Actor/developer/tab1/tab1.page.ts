import { Component } from "@angular/core";
import { ModalController, AlertController, NavController } from "@ionic/angular";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserService } from '../../../service/user.service';
import { ModalTachePage } from '../modalTache/modalTache.page';
import { DetailmodalPage } from '../detailmodal/detailmodal.page';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from '../../../models/user.model';
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { Service } from 'src/app/service/service';

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"]
})
export class Tab1Page {
  tasks: any;
  username: string
  sub
  mainuser: AngularFirestoreDocument;
  taches
  isEdit: false
  Array: any[];

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public modalController: ModalController,
    private servicetache: Service,
    private afs: AngularFirestore,
    private user: UserService,
    private storage: Storage,
    private alertController: AlertController,
    private navController: NavController
  ) {    this.storage.get(CURRENT_USER_KEY).then(
    (currentUser: User) => {
      this.mainuser = afs.doc(`users/${currentUser.id}`)
      this.sub = this.mainuser.valueChanges().subscribe(event => {
        this.username = event.username.split('@')[0]
      }) //get data from firebase 
    }) }

  ngOnInit() {

    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        // console.log("current user id:"+ currentUser.username)
        this.mainuser = this.afs.doc(`users/${currentUser.id}`)
        this.sub = this.mainuser.valueChanges().subscribe(event => {
          this.taches = event.taches
          this.Array = this.taches;
          console.log("comment", this.Array)
        })
      })


  }

  // creation of the modal
  async CreateTache() {
    const modal = await this.modalController.create({
      component: ModalTachePage,
      componentProps: {}
    });
    return await modal.present();
  }

  RemoveTache(tacheId) {
    this.servicetache.delete_Task(tacheId);
  }

  EditTache(tache) {
    tache.isEdit = true;
    tache.nomtache = tache.Nom;
    tache.devemail = tache.Dev;
    tache.projet = tache.projet;
    tache.deadline = tache.Deadline;
    tache.description = tache.description;
    //tache.createur = tache.createur;
  }

  UpdateTache(tacheRow) {
    let tache = {};
    tache["Nom"] = tacheRow.nomtache;
    tache["Dev"] = tacheRow.devemail;
    tache["projet"] = tacheRow.projet;
    tache["Deadline"] = tacheRow.deadline;
    tache["description"] = tacheRow.description;
    //tache["créateur"] = tacheRow.createur;
    this.servicetache.update_Task(tacheRow.id, tache);
    tacheRow.isEdit = false;
  }

  async DetailTache(item) {
    const modal = await this.modalController.create({
      component: DetailmodalPage,
      componentProps: {
        item: item
      }
    });
    return await modal.present();
  }

  signOut() {

    const alert = this.alertController.create({
      header: 'Sign out',
      message: 'Voulez-vous vraiment quitter cette page',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel'
        },
        {
          text: 'Confirmer',
          handler: () => {
            this.user.signOut();
            this.router.navigate(["/login"]);
          }
        }
      ]
    });

    alert.then((alertElement) => alertElement.present());

  }


  filterList(evt) {
    const searchTerm = evt.target.value;
    if (searchTerm && searchTerm.trim() != "") {
      this.Array = this.taches.filter(item => {
        return (
          item.nomtache
        );
      });

    }
  }

}
