import { Component } from "@angular/core";
import { ModalreunionPage } from "../../project-manager/modalreunion/modalreunion.page";
import { ModalController, AlertController } from "@ionic/angular";
import { Service } from "../../../service/service";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { UserService } from 'src/app/service/user.service';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../../../models/user.model';

@Component({
  selector: "app-tab3",
  templateUrl: "tab3.page.html",
  styleUrls: ["tab3.page.scss"]
})
export class Tab3Page {

  reunions: any;
  Array: any[];
  username: string
  sub
  mainuser: AngularFirestoreDocument;
  
  constructor(
    public modalController: ModalController,
    private servicereunion: Service,
    public router: Router,
    public afAuth: AngularFireAuth,
    public alertController: AlertController,
    private storage: Storage,
    private afs: AngularFirestore,
    private user: UserService
  ) 
  {this.storage.get(CURRENT_USER_KEY).then(
    (currentUser: User) => {
      this.mainuser = afs.doc(`users/${currentUser.id}`)
      this.sub = this.mainuser.valueChanges().subscribe(event => {
        this.username = event.username.split('@')[0]
      }) //get data from firebase 
    })}

  ngOnInit() {
    this.servicereunion.read_Reunions().subscribe(data => {
      this.reunions = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Date: e.payload.doc.data()["Date"],
          Dev: e.payload.doc.data()["Dev"],
          client: e.payload.doc.data()["client"],
          projet: e.payload.doc.data()["projet"],
          description: e.payload.doc.data()["description"]
        };
      });
      this.Array = this.reunions;
    });
  }

  async CreateReunion() {
    const modal = await this.modalController.create({
      component: ModalreunionPage,
      componentProps: {}
    });
    return await modal.present();
  }
  RemoveReunion(reunionId) {
    this.servicereunion.delete_Reunion(reunionId);
  }
  EditReunion(reunion) {
    console.log("reunionnnnn", reunion);
    reunion.isEdit = true;
    reunion.datereunion = reunion.Date;
    reunion.devemail = reunion.Dev;
    reunion.projet = reunion.projet;
    reunion.client = reunion.client;
    reunion.description = reunion.description;
    console.log("reunionnnnn", reunion);
  }

  UpdateReunion(reunionRow) {
    let reunion = {};
    reunion["Date"] = reunionRow.datereunion;
    reunion["Dev"] = reunionRow.devemail;
    reunion["projet"] = reunionRow.projet;
    reunion["client"] = reunionRow.client;
    reunion["description"] = reunionRow.description;
    this.servicereunion.update_Reunion(reunionRow.id, reunion);
    reunionRow.isEdit = false;
    console.log("test1", reunionRow);
    console.log("test2", reunion);
  }

  filterList(evt) {
    const searchTerm = evt.target.value;
    if (searchTerm && searchTerm.trim() != "") {
      this.Array = this.reunions.filter(item => {
        return (
          item.projet
        );
      });
    }
  }

  signOut() {

    const alert = this.alertController.create({
        header: 'Sign out',
        message: 'Voulez-vous vraiment quitter cette page',
        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => {
                    this.user.signOut();
                    this.router.navigate(["/login"]);
                }
            }
        ]
    });

    alert.then((alertElement) => alertElement.present());

}
}
