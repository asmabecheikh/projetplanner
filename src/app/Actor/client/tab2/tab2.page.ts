import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { ModalController, AlertController } from "@ionic/angular";

import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Service } from "../../../service/service";
import { UserService } from "../../../service/user.service";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../../../models/user.model';

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  tasks: any;
  username: string;
  sub;
  mainuser: AngularFirestoreDocument;

  @ViewChild("doughnutCanvas", { static: false }) doughnutCanvas;
  @Input() item;
  @Input() tache;
  nbtodo: any = 40;
  nbdoing: any = 30;
  nbdone: any = 30;
  nbtotal;
  doughnutChart: any;
  
  constructor(
    public modalController: ModalController,
    private servicetache: Service,
    private afs: AngularFirestore,
    private user: UserService,
    private storage: Storage,
    public router: Router,
    public afAuth: AngularFireAuth,
    public alertController: AlertController
  ) {
    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = afs.doc(`users/${currentUser.id}`)
        this.sub = this.mainuser.valueChanges().subscribe(event => {
          this.username = event.username.split('@')[0]
        }) //get data from firebase 
      })
  }

  ngOnInit() {
    console.log(this.item);
    console.log(this.tache);
    this.nbtotal = this.tache.length;
    for (let i = 0; i < this.tache.length; i++) {
      console.log("test", this.tache[i]);
      if (this.tache[i].etat === "TODO") {
        this.nbtodo++;
        console.log("test", this.nbtodo);
      } else if (this.tache[i].etat === "DOING") {
        this.nbdoing++;
        console.log("test", this.nbdoing);
      } else {
        this.nbdone++;
        console.log("test", this.nbdone);
      }
    }
  }

  ionViewDidEnter() {
    this.createDoughnutChart();
  }
  createDoughnutChart() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        labels: ["TODO", "DOING", "DONE"],
        datasets: [
          {
            label: "# of Votes",
            data: [this.nbtodo, this.nbdoing, this.nbdone],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)"
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      }
    });
  }

  signOut() {

    const alert = this.alertController.create({
        header: 'Sign out',
        message: 'Voulez-vous vraiment quitter cette page',
        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => {
                    this.user.signOut();
                    this.router.navigate(["/login"]);
                }
            }
        ]
    });

    alert.then((alertElement) => alertElement.present());

}
}
