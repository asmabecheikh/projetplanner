import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../../../models/user.model';
import { MyData } from '../../../interfaces/MyData'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  username: string
  sub
  mainuser: AngularFirestoreDocument;
  
  // Upload Task 
  task: AngularFireUploadTask;

  // Progress in percentage
  percentage: Observable<number>;

  // Snapshot of uploading file
  snapshot: Observable<any>;

  // Uploaded File URL
  UploadedFileURL: Observable<string>;

  //Uploaded Image List
  images: Observable<MyData[]>;

  //File details  
  fileName: string;
  fileSize: number;

  //Status check 
  isUploading: boolean;
  isUploaded: boolean;

  private imageCollection: AngularFirestoreCollection<MyData>;
  
  constructor(
    private storage: AngularFireStorage,
    public alertController: AlertController,
    private database: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    private user: UserService,
    private localstorage: Storage
  ) {
    this.localstorage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = database.doc(`users/${currentUser.id}`)
        this.sub = this.mainuser.valueChanges().subscribe(event => {
          this.username = event.username.split('@')[0]
        }) //get data from firebase 
      })

    this.isUploading = false;
    this.isUploaded = false;
    //Set collection where our documents/ images info will save
    this.imageCollection = database.collection<MyData>('filesUploaded');
    this.images = this.imageCollection.valueChanges();
  }

  async presentAlert(title: string, content: string) {
    const alert = await this.alertController.create({
      header: title,
      message: content,
      buttons: ['OK']
    })

    await alert.present()
  }

  uploadFile(event: FileList) {

    // The File object
    const file = event.item(0)

    // Validation for Images Only
    if (file.type !== 'application/pdf') {
      this.presentAlert("Alerte", "Type de fichier non pris en charge !")
      console.error('unsupported file type : ')
      return;
    }

    this.isUploading = true;
    this.isUploaded = false;


    this.fileName = file.name;

    // The storage path
    const path = `files/${new Date().getTime()}_${file.name}`;

    // Totally optional metadata
    const customMetadata = { app: 'Freaky Image Upload Demo' };

    //File reference
    const fileRef = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, file, { customMetadata });

    // Get file progress percentage
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges().pipe(

      finalize(() => {
        // Get uploaded file storage path
        this.UploadedFileURL = fileRef.getDownloadURL();

        this.UploadedFileURL.subscribe(resp => {
          this.addImagetoDB({
            name: file.name,
            filepath: resp,
            size: this.fileSize
          });
          this.isUploading = false;
          this.isUploaded = true;
        }, error => {
          console.error(error);
        })
      }),
      tap(snap => {
        this.fileSize = snap.totalBytes;
      })
    )
  }

  addImagetoDB(image: MyData) {
    //Create an ID for document
    const id = this.database.createId();

    //Set document id with value in database
    this.imageCollection.doc(id).set(image).then(resp => {
      console.log(resp);
    }).catch(error => {
      console.log("error " + error);
    });
  }

  signOut() {

    const alert = this.alertController.create({
      header: 'Sign out',
      message: 'Voulez-vous vraiment quitter cette page',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel'
        },
        {
          text: 'Confirmer',
          handler: () => {
            this.user.signOut();
            this.router.navigate(["/login"]);
          }
        }
      ]
    });

    alert.then((alertElement) => alertElement.present());

  }


}
