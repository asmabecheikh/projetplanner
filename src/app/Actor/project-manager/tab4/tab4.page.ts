import { Component } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { User } from "../../../models/user.model";
import { CURRENT_USER_KEY } from "../../storage.constant";
import { Storage } from "@ionic/storage";
import { CrudprojectServiceService } from "src/app/service/crudproject-service.service";
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: "app-tab4",
  templateUrl: "tab4.page.html",
  styleUrls: ["tab4.page.scss"]
})
export class Tab4Page {
  reunions: any[];
  Array: any[];
  messageText: any;
  connected = false;
  User: any;
  username: string;
  sub;
  mainuser: AngularFirestoreDocument;
  public messages: any = [];

  constructor(
    public modalController: ModalController,
    private service: CrudprojectServiceService,
    public alertController: AlertController,
    private user: UserService,
    public router: Router,
    private afs: AngularFirestore,
    private storage: Storage
  ) 
  { this.storage.get(CURRENT_USER_KEY).then(
    (currentUser: User) => {
      this.mainuser = afs.doc(`users/${currentUser.id}`)
      this.sub = this.mainuser.valueChanges().subscribe(event => {
        this.username = event.username.split('@')[0]
      }) //get data from firebase 
    })}

  ngOnInit() {
    this.storage.get(CURRENT_USER_KEY).then((currentUser: User) => {
      console.log("current user id:" + currentUser.username);
      this.User = currentUser;
      this.connected = true;
    });
    this.service.getmessage().subscribe(data => {
      this.messages = data.map(e => {
        return {
          id: e.payload.doc.data()["iduser"],
          text: e.payload.doc.data()["text"],
          date: e.payload.doc.data()["date"]
        };
      });
    });
  }

  sendMessage() {
    let msg = {};
    msg["iduser"] = this.User.id;
    msg["text"] = this.messageText;
    msg["date"] = new Date().toISOString();
    this.service.sendmessage(msg);
    this.messageText = "";
  }

  /////logout connected = false;
  signOut() {

    const alert = this.alertController.create({
        header: 'Sign out',
        message: 'Voulez-vous vraiment quitter cette page',
        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => {
                    this.user.signOut();
                    this.router.navigate(["/login"]);
                }
            }
        ]
    });

    alert.then((alertElement) => alertElement.present());

}
}
