import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { Chart } from "chart.js";
@Component({
  selector: "app-detailproject",
  templateUrl: "./detailproject.page.html",
  styleUrls: ["./detailproject.page.scss"]
})
export class DetailprojectPage implements OnInit {
  @ViewChild("doughnutCanvas", { static: false }) doughnutCanvas;
  @Input() item;
  @Input() tache;
  nbtodo: any = 40;
  nbdoing: any = 30;
  nbdone: any = 30;
  nbtotal;
  doughnutChart: any;
  ionViewDidEnter() {
    this.createDoughnutChart();
  }
  createDoughnutChart() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        labels: ["TODO", "DOING", "DONE"],
        datasets: [
          {
            label: "# of Votes",
            data: [this.nbtodo, this.nbdoing, this.nbdone],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)"
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      }
    });
  }

  constructor(private modalController: ModalController) {}

  ngOnInit() {
    console.log(this.item);
    console.log(this.tache);
    this.nbtotal = this.tache.length;
    for (let i = 0; i < this.tache.length; i++) {
      console.log("test", this.tache[i]);
      if (this.tache[i].etat === "TODO") {
        this.nbtodo++;
        console.log("test", this.nbtodo);
      } else if (this.tache[i].etat === "DOING") {
        this.nbdoing++;
        console.log("test", this.nbdoing);
      } else {
        this.nbdone++;
        console.log("test", this.nbdone);
      }
    }
  }
  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
