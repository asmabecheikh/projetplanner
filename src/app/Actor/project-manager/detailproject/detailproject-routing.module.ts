import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailprojectPage } from './detailproject.page';

const routes: Routes = [
  {
    path: '',
    component: DetailprojectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailprojectPageRoutingModule {}
