import { Component } from "@angular/core";
import { CreateprojectModalPage } from "../createproject-modal/createproject-modal.page";
import { ModalController, AlertController } from "@ionic/angular";
import { CrudprojectServiceService } from "../../../service/crudproject-service.service";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../../../models/user.model';
import { UserService } from "../../../service/user.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { DetailprojectPage } from "../detailproject/detailproject.page";
import { Service } from "../../../service/service";

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"]
})
export class Tab1Page {
  projectid: string;
  mainuser: AngularFirestoreDocument;
  sub;
  projects: any[];
  taches: any[];
  Array: any[];
  posts;
  userPosts;
  username: string;


  constructor(
    public modalController: ModalController,
    private service: CrudprojectServiceService,
    private tacheservice: Service,
    private afs: AngularFirestore,
    private storage: Storage,
    private user: UserService,
    public router: Router,
    public afAuth: AngularFireAuth,
    public alertController: AlertController
  ) {
    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = afs.doc(`users/${currentUser.id}`)
        this.sub = this.mainuser.valueChanges().subscribe(event => {
          this.username = event.username.split('@')[0]
        }) //get data from firebase 
      })
  }

  ngOnInit() {
    this.service.read_Projets().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nomProjet: e.payload.doc.data()["nomProjet"],
          deadline: e.payload.doc.data()["deadline"],
          description: e.payload.doc.data()["description"],
          budget: e.payload.doc.data()["budget"],
          etat: e.payload.doc.data()["etat"]
        };
      });
      //console.log(this.projects);
      // this.projectid=this.projects.id;

      this.Array = this.projects;
      for (let i = 0; i < this.projects.length; i++) {
        this.tacheservice.read_Tasks().subscribe(data => {
          this.taches = data
            .map(a => {
              return {
                id: a.payload.doc.id,
                isEdit: false,
                Nom: a.payload.doc.data()["Nom"],
                Deadline: a.payload.doc.data()["Deadline"],
                Dev: a.payload.doc.data()["Dev"],
                projet: a.payload.doc.data()["projet"],
                createur: a.payload.doc.data()["créateur"],
                description: a.payload.doc.data()["description"],
                etat: a.payload.doc.data()["etat"],
                affectation: a.payload.doc.data()["affectation"]
              };
            })
            .filter(a => a.projet === this.projects[i].id);
          console.log(this.taches);
        });
      }
    });
  }

  async createProject() {
    const modal = await this.modalController.create({
      component: CreateprojectModalPage,
      componentProps: {}
    });
    return await modal.present();
  }

  removeProject(projectId) {
    this.service.delete_Projet(projectId);
  }

  updateProject(projectRow) {
    let projects = {};
    projects["nomProjet"] = projectRow.projectName;
    projects["deadline"] = projectRow.deadline;
    projects["description"] = projectRow.description;
    projects["budget"] = projectRow.budget;
    projects["etat"] = projectRow.etat;
    this.service.update_Projet(projectRow.id, projects);
    projectRow.isEdit = false;
  }

  editProject(project) {
    project.isEdit = true;
    project.projectName = project.nomProjet;
    project.deadline = project.deadline;
    project.description = project.description;
    project.budget = project.budget;
    project.etat = project.etat;
    console.log("Updated", project);
  }

  signOut() {

    const alert = this.alertController.create({
      header: 'Sign out',
      message: 'Voulez-vous vraiment quitter cette page',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel'
        },
        {
          text: 'Confirmer',
          handler: () => {
            this.user.signOut();
            this.router.navigate(["/login"]);
          }
        }
      ]
    });

    alert.then((alertElement) => alertElement.present());

  }

  filterList(evt) {
    const searchTerm = evt.target.value;
    console.log(searchTerm);
    console.log(this.projects);
    if (searchTerm && searchTerm.trim() != "") {
      this.Array = this.projects.filter(item => {
        console.log(item.nomProjet);
        return (
          item.nomProjet.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        );
      });
      console.log("msg", searchTerm, this.Array);
    }
  }

  async detailProject(item) {
    const modal = await this.modalController.create({
      component: DetailprojectPage,
      componentProps: {
        item: item,
        tache: this.taches
      }
    });
    return await modal.present();
  }
}
