import { Component } from "@angular/core";
import { ModalPage } from "../modal/modal.page";
import { ModalController, AlertController } from "@ionic/angular";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Service } from "../../../service/service";
import { UserService } from "../../../service/user.service";
import { DetailstacheModalPage } from "../detailstache-modal/detailstache-modal.page";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { CURRENT_USER_KEY } from '../../storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../../../models/user.model';

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  tasks: any[];
  Array: any[];
  username: string;
  sub;
  mainuser: AngularFirestoreDocument;

  constructor(
    public modalController: ModalController,
    private servicetache: Service,
    private afs: AngularFirestore,
    private user: UserService,
    public router: Router,
    public afAuth: AngularFireAuth,
    private storage: Storage,
    public alertController: AlertController
  ) {
    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = afs.doc(`users/${currentUser.id}`)
        this.sub = this.mainuser.valueChanges().subscribe(event => {
          this.username = event.username.split('@')[0]
        }) //get data from firebase 
      })
  }

  ngOnInit() {
    this.servicetache.read_Tasks().subscribe(data => {
      this.tasks = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Nom: e.payload.doc.data()["Nom"],
          Deadline: e.payload.doc.data()["Deadline"],
          Dev: e.payload.doc.data()["Dev"],
          projet: e.payload.doc.data()["projet"],
          createur: e.payload.doc.data()["créateur"],
          description: e.payload.doc.data()["description"],
          etat: e.payload.doc.data()["etat"],
          affectation: e.payload.doc.data()["affectation"]
        };
      });
      console.log(this.tasks);
      this.Array = this.tasks;
    });
  }

  // creation of the modal
  async CreateTache() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {}
    });
    return await modal.present();
  }
  RemoveTache(tacheId) {
    this.servicetache.delete_Task(tacheId);
  }
  EditTache(tache) {
    tache.isEdit = true;
    tache.nomtache = tache.Nom;
    tache.devemail = tache.Dev;
    tache.projet = tache.projet;
    tache.deadline = tache.Deadline;
    tache.createur = tache.createur;
    console.log("tacheeeeeeeee", tache);
  }

  UpdateTache(tacheRow) {
    let tache = {};
    tache["Nom"] = tacheRow.nomtache;
    tache["Dev"] = tacheRow.devemail;
    tache["projet"] = tacheRow.projet;
    tache["Deadline"] = tacheRow.deadline;
    tache["créateur"] = tacheRow.createur;
    this.servicetache.update_Task(tacheRow.id, tache);
    tacheRow.isEdit = false;
  }
  filterList(evt) {
    const searchTerm = evt.target.value;
    console.log(searchTerm);
    console.log(this.tasks);
    if (searchTerm && searchTerm.trim() != "") {
      this.Array = this.tasks.filter(item => {
        console.log(item.Nom);
        return item.Nom.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
      console.log("msg", searchTerm, this.Array);
    }
  }

  async DetailTache(item) {
    const modal = await this.modalController.create({
      component: DetailstacheModalPage,
      componentProps: {
        item: item
      }
    });
    return await modal.present();
  }
  signOut() {

    const alert = this.alertController.create({
        header: 'Sign out',
        message: 'Voulez-vous vraiment quitter cette page',
        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => {
                    this.user.signOut();
                    this.router.navigate(["/login"]);
                }
            }
        ]
    });

    alert.then((alertElement) => alertElement.present());

}
}
