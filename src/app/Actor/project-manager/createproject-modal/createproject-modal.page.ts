import { Component, OnInit } from "@angular/core";
import { CrudprojectServiceService } from "../../../service/crudproject-service.service";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-createproject-modal",
  templateUrl: "./createproject-modal.page.html",
  styleUrls: ["./createproject-modal.page.scss"]
})
export class CreateprojectModalPage implements OnInit {
  projects: any;
  projectName: string;
  description: string;
  deadline = new Date().toISOString();
  budget: string;
  etat: string;
  constructor(
    private service: CrudprojectServiceService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  CreateProject() {
    let projet = {};
    projet["nomProjet"] = this.projectName;
    projet["description"] = this.description;
    projet["deadline"] = this.deadline;
    projet["budget"] = this.budget;
    projet["etat"] = "TODO";
    console.log("test", projet);

    this.service.create_NewProjet(projet);
    this.modalController.dismiss();
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
