import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateprojectModalPageRoutingModule } from './createproject-modal-routing.module';

import { CreateprojectModalPage } from './createproject-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateprojectModalPageRoutingModule
  ],
  declarations: [CreateprojectModalPage]
})
export class CreateprojectModalPageModule {}
