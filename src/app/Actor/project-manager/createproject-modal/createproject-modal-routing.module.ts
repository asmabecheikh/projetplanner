import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateprojectModalPage } from './createproject-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreateprojectModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateprojectModalPageRoutingModule {}
