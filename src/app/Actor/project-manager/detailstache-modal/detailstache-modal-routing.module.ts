import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailstacheModalPage } from './detailstache-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DetailstacheModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailstacheModalPageRoutingModule {}
