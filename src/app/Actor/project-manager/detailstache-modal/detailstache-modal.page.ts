import { Component, OnInit, Input } from "@angular/core";
import { NavParams } from "@ionic/angular";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-detailstache-modal",
  templateUrl: "./detailstache-modal.page.html",
  styleUrls: ["./detailstache-modal.page.scss"]
})
export class DetailstacheModalPage implements OnInit {
  @Input() item;
  constructor(
    private navParams: NavParams,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    console.log(this.item);
  }
  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
