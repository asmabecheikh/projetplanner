import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailstacheModalPageRoutingModule } from './detailstache-modal-routing.module';

import { DetailstacheModalPage } from './detailstache-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailstacheModalPageRoutingModule
  ],
  declarations: [DetailstacheModalPage]
})
export class DetailstacheModalPageModule {}
