import { Component, OnInit } from "@angular/core";
import { Service } from "../../../service/service";
import { ModalController } from "@ionic/angular";
import { CrudprojectServiceService } from "src/app/service/crudproject-service.service";
import { firestore } from "firebase/app";
import { AngularFirestore } from "@angular/fire/firestore";
import { User } from "src/app/models/user.model";
import { CURRENT_USER_KEY } from "../../storage.constant";
import { Storage } from "@ionic/storage";
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: "app-modal",
  templateUrl: "./modal.page.html",
  styleUrls: ["./modal.page.scss"]
})
export class ModalPage implements OnInit {
  tasks: any;
  projects: any[];
  nomtache: string;
  devemail: string = "";
  projet: string;
  deadline: string;
  createur: string;
  description: string;
  affecter: boolean = false;
  etat: string;
  affectation: string;
  current_user: any;
  current_dev: any;
  constructor(
    private serviceproject: CrudprojectServiceService,
    private servicetache: Service,
    private modalController: ModalController,
    public afstore: AngularFirestore,
    private storage: Storage,
    private user: UserService
  ) { }

  ngOnInit() {
    this.servicetache.getidbyusername(this.devemail)
    console.log("dddd",this.servicetache.getidbyusername(this.devemail))

    this.serviceproject.read_Projets().subscribe(data => {
      this.projects = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nomProjet: e.payload.doc.data()["nomProjet"],
          deadline: e.payload.doc.data()["deadline"],
          description: e.payload.doc.data()["description"],
          budget: e.payload.doc.data()["budget"],
          etat: e.payload.doc.data()["etat"]
        };
      });
    });
  }
  onChange(event) {
    // console.log(event.detail.value.id);
    this.projet = event.detail.value.id;
  }

  Creertache() {
    let tache = {};
    tache["Nom"] = this.nomtache;
    tache["Dev"] = this.devemail;
    tache["projet"] = this.projet;
    tache["Deadline"] = this.deadline;
    //tache["créateur"] = this.current_user;
    tache["etat"] = "ToDo";
    tache["description"] = this.description;
    if (this.devemail != "") {
      tache["affectaion"] = "affecté";
      this.afstore.doc(`users/${this.current_dev}`).update({
        taches: tache
      });
    } else {
      tache["affectaion"] = "Non_affecté";
    }

    console.log("test", tache);

    this.servicetache.create_Newtask(tache);
    this.servicetache.getidbyusername(this.devemail)
  
   /* this.current_user.subscribe(
      (user: User) => {
        if (user.profileType == "DEVELOPER") {
          console.log(user.username)
          this.afstore.collection("tacheDevelopper").add(tache),
          this.afstore.collection("tacheDevelopper").add(user.id)
        }
      }) */
    this.modalController.dismiss();
  }

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
