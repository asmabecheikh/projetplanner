import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalreunionPage } from './modalreunion.page';

describe('ModalreunionPage', () => {
  let component: ModalreunionPage;
  let fixture: ComponentFixture<ModalreunionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalreunionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalreunionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
