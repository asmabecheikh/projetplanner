import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalreunionPageRoutingModule } from './modalreunion-routing.module';

import { ModalreunionPage } from './modalreunion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalreunionPageRoutingModule
  ],
  declarations: [ModalreunionPage]
})
export class ModalreunionPageModule {}
