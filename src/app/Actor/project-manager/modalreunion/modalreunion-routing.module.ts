import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalreunionPage } from './modalreunion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalreunionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalreunionPageRoutingModule {}
