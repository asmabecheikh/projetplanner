import { Component, OnInit } from "@angular/core";
import { Service } from "../../../service/service";
import { from } from "rxjs";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-modalreunion",
  templateUrl: "./modalreunion.page.html",
  styleUrls: ["./modalreunion.page.scss"]
})
export class ModalreunionPage implements OnInit {
  reunions: any;
  datereunion: string;
  devemail: string;
  projet: string;
  client: string;
  description: string;
  constructor(
    private servicereunion: Service,
    private modalController: ModalController
  ) {}

  ngOnInit() {}
  Creerreunion() {
    let reunion = {};
    reunion["Date"] = this.datereunion;
    reunion["Dev"] = this.devemail;
    reunion["projet"] = this.projet;
    reunion["client"] = this.client;
    reunion["description"] = this.description;

    this.servicereunion.create_NewReunion(reunion);
    this.modalController.dismiss({
      dismissed: true
    });
  }
  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
