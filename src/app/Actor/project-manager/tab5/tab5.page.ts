import { Component } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { User } from "../../../models/user.model";
import { CURRENT_USER_KEY } from "../../storage.constant";
import { Storage } from "@ionic/storage";
import { CrudprojectServiceService } from "src/app/service/crudproject-service.service";
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MyData } from '../../../interfaces/MyData';

@Component({
  selector: "app-tab5",
  templateUrl: "tab5.page.html",
  styleUrls: ["tab5.page.scss"]
})
export class Tab5Page {
  //Uploaded Image List
  images: Observable<MyData[]>;

  User: any;
  username: string;
  sub;
  mainuser: AngularFirestoreDocument;
  public messages: any = [];
  private imageCollection: AngularFirestoreCollection<MyData>;

  constructor(
    public modalController: ModalController,
    public alertController: AlertController,
    private user: UserService,
    public router: Router,
    private afs: AngularFirestore,
    private storage: Storage
  ) 
  { this.storage.get(CURRENT_USER_KEY).then(
    (currentUser: User) => {
      this.mainuser = afs.doc(`users/${currentUser.id}`)
      this.sub = this.mainuser.valueChanges().subscribe(event => {
        this.username = event.username.split('@')[0]
      }) //get data from firebase 
    })
    this.imageCollection = afs.collection<MyData>('filesUploaded');
    this.images = this.imageCollection.valueChanges();
  }

  ngOnInit() {}


  /////logout connected = false;
  signOut() {

    const alert = this.alertController.create({
        header: 'Sign out',
        message: 'Voulez-vous vraiment quitter cette page',
        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => {
                    this.user.signOut();
                    this.router.navigate(["/login"]);
                }
            }
        ]
    });

    alert.then((alertElement) => alertElement.present());

}
}
