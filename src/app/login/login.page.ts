import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
//import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { User } from '../models/user.model';
import { Storage } from '@ionic/storage';
import { CURRENT_USER_KEY } from '../Actor/storage.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = "";
  password: string = "";
  //profileType: ProfileTypes = null;
  currentUser: User = new User();

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public alertController: AlertController,
    private storage: Storage,
    public user: UserService
  ) { }

  ngOnInit() {
  }

  async presentAlert(title: string, content: string) {
    const alert = await this.alertController.create({
      header: title,
      message: content,
      buttons: ['OK']
    })

    await alert.present()
  }

  async login() {
    if (this.username == "" || this.password == "") {
      this.presentAlert("Alerte", "Veuillez remplir les champs !")
    }
    const { username, password } = this;
    try {
      const res = await this.afAuth.auth.signInWithEmailAndPassword(username, password).then(
        (result: firebase.auth.UserCredential) => {
          this.user.getUID(result.user.uid).subscribe(
            (user: User) => {
              this.storage.set(CURRENT_USER_KEY, user);
              this.navigateToCurrentUserPage()
            })
        })

    } catch (err) {
      console.dir(err)
      if (err.code === "auth/user-not-found") {
        this.presentAlert('Alerte', 'Utilisateur non trouvé!')
        console.log("User not found");
      }else if(err.code === "auth/wrong-password"){
        this.presentAlert('Alerte', 'le mot de passe n\'est pas valide pour l\'e-mail donné!')
        console.log("the password is invalid");
      }
    }
  }

  navigateToCurrentUserPage() {

    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.currentUser = currentUser;
        console.log("current user:", this.currentUser);
       this.router.navigate(['/' + this.currentUser.profileType.toLowerCase().replace('_', '-')+"/tabs/tab1"]);
        //this.router.navigate(["/project-manager/tabs/tab1"])
        console.log('/' + this.currentUser.profileType.toLowerCase().replace('_', '-'));
      })

  }

  async signUp() {
    this.router.navigate(['/register'])
  }

}
