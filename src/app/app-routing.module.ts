import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthService } from "./service/auth.service";
import { AngularFireAuthGuard, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';

const REDIRECT_UNAUTHORIZED_TO_LOGIN = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "project-manager",
    loadChildren: () =>
      import("./Actor/project-manager/tabs/tabs.module").then(
        m => m.TabsPageModule
      ),
      canActivate: [ AngularFireAuthGuard ],
      data: { authGuardPipe: REDIRECT_UNAUTHORIZED_TO_LOGIN }
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then(m => m.LoginPageModule)
  },
  {
    path: "register",
    loadChildren: () =>
      import("./register/register.module").then(m => m.RegisterPageModule)
  },
  {
    path: "client",
    loadChildren: () =>
      import("./Actor/client/tabs/tabs.module").then(m => m.TabsPageModule),
      canActivate: [ AngularFireAuthGuard ],
      data: { authGuardPipe: REDIRECT_UNAUTHORIZED_TO_LOGIN }
  },
  {
    path: "developer",
    loadChildren: () =>
      import("./Actor/developer/tabs/tabs.module").then(m => m.TabsPageModule),
      canActivate: [ AngularFireAuthGuard ],
      data: { authGuardPipe: REDIRECT_UNAUTHORIZED_TO_LOGIN}
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
