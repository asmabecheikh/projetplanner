import { Injectable } from '@angular/core'
import { Router, CanActivate } from '@angular/router'
import { UserService } from './user.service'


@Injectable()
export class AuthService implements CanActivate {

	constructor(private router: Router, private User: UserService) {

	}

	async canActivate(route) {
		if(await this.User.isAuthenticated()) {
		// if the user is authentificated then true
			return true
		}
		// otherwise we redirect user to login page 
		this.router.navigate(['/login'])
		return false
	}
}