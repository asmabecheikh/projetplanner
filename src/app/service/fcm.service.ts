import { Injectable } from "@angular/core";
import { Platform } from "@ionic/angular";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
@Injectable({
  providedIn: "root"
})
export class FcmService {
  constructor(private afs: AngularFirestore, private platform: Platform) {}
  // Get permission from the user
  async getToken() {}

  // Save the token to firestore
  private saveTokenToFirestore(token) {}

  // Listen to incoming FCM messages
  listenToNotifications() {}
}
