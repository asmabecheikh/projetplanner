import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from '../models/user.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { CURRENT_USER_KEY } from '../Actor/storage.constant';

/* interface user {
	username: string,
	profileType: ProfileTypes,
	uid: string //unique id for a particualr user
} */

@Injectable({
	providedIn: 'root'
})
export class UserService {
	private user: User

	constructor(
		private afAuth: AngularFireAuth,
		private afstore: AngularFirestore,
		private storage: Storage
		) { }


	getUID(uid: string): Observable<User> {

        return this.afstore.doc<User>('/users/' + uid).valueChanges();

    }

	addUser(user: User): Promise<void> {

        return this.afstore.doc<User>('/users/' + user.id).set({ ...user });

	}

    getUsersByUserUsername(username: string): any {

        return this.afstore.collection<User>('/users', ref => ref.where('username', '==', username)).valueChanges();

    }
	
	signOut() {

        this.afAuth.auth.signOut();
        this.storage.remove(CURRENT_USER_KEY);

    }

/* 	async isAuthenticated() {
	 	if (this.user) return true

		const user = await this.afAuth.authState.pipe(first()).toPromise()

		return false 
	} */

	isAuthenticated(): Observable<firebase.User> {

        return this.afAuth.authState;

    }


}