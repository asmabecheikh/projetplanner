import { Injectable } from "@angular/core";

import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { UserService } from './user.service';
import { CURRENT_USER_KEY } from '../Actor/storage.constant';
import { firestore } from 'firebase/app';
import { User } from '../models/user.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: "root"
})
export class Service {

  sub
  mainuser: AngularFirestoreDocument;
  taches
  
  constructor(
    private firestore: AngularFirestore,
    public user: UserService,
    private storage: Storage
    ) {}

  create_Newtask(tache) {
    return this.firestore.collection("tache").add(tache);
  }

  create_Newtask1(tache) {
    return this.firestore.collection("tacheDevelopper").add(tache);
  }

  read_Tasks() {
    return this.firestore.collection("tache").snapshotChanges();
  }

  update_Task(tacheID, tache) {
    this.firestore.doc("tache/" + tacheID).update(tache);
  }

  delete_Task(tache_id) {
    this.firestore.doc("tache/" + tache_id).delete()
  }
  create_NewReunion(reunion) {
    return this.firestore.collection("reunion").add(reunion);
  }
  read_Reunions() {
    return this.firestore.collection("reunion").snapshotChanges();
  }

  update_Reunion(reunionID, reunion) {
    this.firestore.doc("reunion/" + reunionID).update(reunion);
  }

  delete_Reunion(reunion_id) {
    this.firestore.doc("reunion/" + reunion_id).delete();
  }
  getidbyusername(username) {
    return this.firestore
      .collection("users", ref => ref.where("username", "==", username))
      .valueChanges();
  }
}
