import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root"
})
export class CrudprojectServiceService {
  constructor(private firestore: AngularFirestore) {}

  create_NewProjet(projet) {
    return this.firestore.collection("projets").add(projet);
  }

  read_Projets() {
    return this.firestore.collection("projets").snapshotChanges();
  }

  update_Projet(projetID, projet) {
    this.firestore.doc("projets/" + projetID).update(projet);
  }

  delete_Projet(projet_id) {
    this.firestore.doc("projets/" + projet_id).delete();
  }
  sendmessage(message) {
    return this.firestore.collection("message").add(message);
  }
  getmessage() {
    return this.firestore.collection("message").snapshotChanges();
  }
}
