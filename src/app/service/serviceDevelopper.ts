import { Injectable } from "@angular/core";

import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { UserService } from './user.service';
import { CURRENT_USER_KEY } from '../Actor/storage.constant';
import { Storage } from '@ionic/storage';
import { User } from '../models/user.model';
import { firestore } from 'firebase/app';

@Injectable({
  providedIn: "root"
})
export class ServiceDevelopper {

  sub
  mainuser: AngularFirestoreDocument;
  taches

  constructor(
    private firestore: AngularFirestore,
    public user: UserService,
    private storage: Storage,
  ) { }

  create_NewtaskDevelopper(tache) {
    return this.firestore.collection("tacheDevelopper").add(tache);
  }

  read_TasksDevelopper() {
    return this.firestore.collection("tacheDevelopper").snapshotChanges();
  }

  update_TaskDevelopper(tacheID, tache) {
    this.firestore.doc("tacheDevelopper/" + tacheID).update(tache);

    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = this.firestore.doc(`users/${currentUser.id}`)
        this.mainuser.update({
          taches: tache
        })
      })
  }

  delete_TaskDevelopper(tache_id) {
    this.firestore.doc("tacheDevelopper/" + tache_id).delete();

    this.storage.get(CURRENT_USER_KEY).then(
      (currentUser: User) => {
        this.mainuser = this.firestore.doc(`users/${currentUser.id}`)
        this.mainuser.update({
          taches: firestore.FieldValue.delete()
        })
      })
  }
}