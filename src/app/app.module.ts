import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ModalPage } from "./Actor/project-manager/modal/modal.page";
import { ModalreunionPage } from "./Actor/project-manager/modalreunion/modalreunion.page";
import { ModalTachePage } from "./Actor/developer/modalTache/modalTache.page";
import { from } from "rxjs";
import { HttpClientModule } from "@angular/common/http";

import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';

// environment
import { environment } from "../environments/environment";
import { UserService } from "./service/user.service";
import { AuthService } from "./service/auth.service";
import { CreateprojectModalPage } from "./Actor/project-manager/createproject-modal/createproject-modal.page";
import { IonicStorageModule } from "@ionic/storage";
import { DetailstacheModalPage } from "./Actor/project-manager/detailstache-modal/detailstache-modal.page";
import { DetailprojectPage } from "./Actor/project-manager/detailproject/detailproject.page";
import { DetailmodalPage } from "../app/Actor/developer/detailmodal/detailmodal.page";
// FCM
import { FCM } from "@ionic-native/fcm/ngx";
@NgModule({
  declarations: [
    AppComponent,
    ModalPage,
    CreateprojectModalPage,
    ModalreunionPage,
    DetailstacheModalPage,
    DetailprojectPage,
    ModalTachePage,
    DetailmodalPage
  ],
  entryComponents: [
    ModalPage,
    CreateprojectModalPage,
    ModalreunionPage,
    DetailstacheModalPage,
    DetailprojectPage,
    ModalTachePage,
    DetailmodalPage
  ],
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireAuthGuardModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    UserService,
    AuthService,
    FCM
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
