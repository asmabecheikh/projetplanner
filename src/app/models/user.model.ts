import { ProfileTypes } from '../enumerations/profile-types';


export class User {

    id: string;
    username: string;
    profileType: ProfileTypes;
}
