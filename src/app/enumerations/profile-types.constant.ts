import { ProfileTypes } from './profile-types';


export const PROFILE_TYPES: ProfileTypes[] = [
    ProfileTypes.PROJECT_MANAGER,
    ProfileTypes.DEVELOPER,
    ProfileTypes.CLIENT
];
